resource "kubernetes_service" "web" {
  metadata {
    name      = "${local.instance_name}-web"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
  }
  spec {
    type = "ClusterIP"
    port {
      port        = 3000
      target_port = "http"
      protocol    = "TCP"
      name        = "http"
    }

    selector = {
      "app.kubernetes.io/name"      = "mastodon"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "web"
    }
  }
}
