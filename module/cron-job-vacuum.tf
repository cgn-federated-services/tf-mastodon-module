resource "kubernetes_cron_job_v1" "vacuum" {
  count = var.vacuum_job_enabled ? 1 : 0
  metadata {
    name      = "vacuum"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
    labels = {
      "app.kubernetes.io/name"      = "mastodon"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "vacuum"
    }
  }
  spec {
    schedule           = "@daily"
    concurrency_policy = "Forbid"
    job_template {
      metadata {}
      spec {
        backoff_limit = 1
        template {
          metadata {}
          spec {
            restart_policy = "Never"
            container {
              name  = "vacuum"
              image = "ghcr.io/mastodon/mastodon:${var.mastodon_version}"
              command = [
                "/bin/bash",
                "-c",
                <<-EOT
                tootctl media remove --days 90
                tootctl preview_cards remove --days 90
                EOT
              ]

              env {
                name  = "S3_READ_TIMEOUT"
                value = "40"
              }

              env_from {
                config_map_ref {
                  name = kubernetes_config_map.env.metadata.0.name
                }
              }

              env_from {
                secret_ref {
                  name = kubernetes_secret.env.metadata.0.name
                }
              }
            }
          }
        }
      }
    }
  }
  depends_on = [kubernetes_job.pre_migrations]
}
