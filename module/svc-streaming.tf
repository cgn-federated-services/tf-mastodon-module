resource "kubernetes_service" "streaming" {
  metadata {
    name      = "${local.instance_name}-streaming"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
  }
  spec {
    type = "ClusterIP"
    port {
      port        = 4000
      target_port = "streaming"
      protocol    = "TCP"
      name        = "streaming"
    }

    selector = {
      "app.kubernetes.io/name"      = "mastodon"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "streaming"
    }
  }
}
