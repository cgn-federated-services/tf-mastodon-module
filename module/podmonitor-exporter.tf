resource "kubernetes_manifest" "podmonitor_exporter" {
  manifest = {
    "apiVersion" = "monitoring.coreos.com/v1"
    "kind"       = "PodMonitor"
    "metadata" = {
      "name"      = "mastodon-exporter"
      "namespace" = kubernetes_namespace.mastodon.metadata.0.name
    }
    "spec" = {
      "podMetricsEndpoints" = [
        {
          "port" = "metrics"
        },
      ]
      "selector" = {
        "matchLabels" = {
          "app.kubernetes.io/component" = "exporter"
        }
      }
    }
  }
}
