resource "kubernetes_namespace" "mastodon" {
  metadata {
    name = "ms-${local.instance_name}"
  }
}
