resource "kubernetes_manifest" "podmonitor_streaming" {
  manifest = {
    "apiVersion" = "monitoring.coreos.com/v1"
    "kind"       = "PodMonitor"
    "metadata" = {
      "name"      = "mastodon-streaming"
      "namespace" = kubernetes_namespace.mastodon.metadata.0.name
    }
    "spec" = {
      "podMetricsEndpoints" = [
        {
          "port" = "streaming"
        },
      ]
      "selector" = {
        "matchLabels" = {
          "app.kubernetes.io/component" = "streaming"
        }
      }
    }
  }
}
