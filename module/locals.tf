locals {
  instance_name = replace(var.instance_hostname, ".", "-")
}
