resource "kubernetes_deployment" "sidekiq_exporter" {
  metadata {
    name      = "sidekiq-exporter"
    namespace = kubernetes_namespace.mastodon.metadata.0.name

    labels = {
      "app.kubernetes.io/name"      = "sidekiq-exporter"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "exporter"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        "app.kubernetes.io/name"      = "sidekiq-exporter"
        "app.kubernetes.io/instance"  = local.instance_name
        "app.kubernetes.io/component" = "exporter"
      }
    }

    template {
      metadata {
        labels = {
          "app.kubernetes.io/name"      = "sidekiq-exporter"
          "app.kubernetes.io/instance"  = local.instance_name
          "app.kubernetes.io/component" = "exporter"
        }
      }

      spec {
        container {
          name  = "sidekiq-exporter"
          image = "docker.io/strech/sidekiq-prometheus-exporter:0.2.0-4"

          port {
            name           = "metrics"
            container_port = 9292
          }
          env {
            name  = "REDIS_URL"
            value = "redis://${var.redis_host}:${var.redis_port}"
          }
        }
      }
    }
  }
}
