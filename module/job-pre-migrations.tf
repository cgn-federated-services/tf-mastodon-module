resource "kubernetes_job" "pre_migrations" {
  metadata {
    name      = "pre-migrations"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
    labels = {
      "app.kubernetes.io/name"      = "mastodon"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/version"   = var.mastodon_version
      "app.kubernetes.io/component" = "pre-migrations"
    }
  }
  spec {
    template {
      metadata {
        name = "pre-migrations"
      }
      spec {
        restart_policy = "Never"
        container {
          name  = "pre-migrations"
          image = "ghcr.io/mastodon/mastodon:${var.mastodon_version}"
          command = [
            "bundle",
            "exec",
            "rake",
            "db:migrate"
          ]

          env_from {
            config_map_ref {
              name = kubernetes_config_map.env.metadata.0.name
            }
          }

          env_from {
            secret_ref {
              name = kubernetes_secret.env.metadata.0.name
            }
          }

          env {
            name  = "DB_PORT"
            value = var.postgres_port
          }

          env {
            name  = "PORT"
            value = "3000"
          }

          env {
            name  = "SKIP_POST_DEPLOYMENT_MIGRATIONS"
            value = true
          }
        }
      }
    }
  }
  wait_for_completion = true
  timeouts {
    create = "3m"
    update = "3m"
  }
}
