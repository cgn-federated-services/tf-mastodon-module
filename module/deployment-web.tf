resource "kubernetes_deployment" "web" {
  metadata {
    name      = "web"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
    labels = {
      "app.kubernetes.io/name"      = "mastodon"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "web"
      "app.kubernetes.io/part-of"   = "rails"
    }
  }
  spec {
    selector {
      match_labels = {
        "app.kubernetes.io/name"      = "mastodon"
        "app.kubernetes.io/instance"  = local.instance_name
        "app.kubernetes.io/component" = "web"
        "app.kubernetes.io/part-of"   = "rails"
      }
    }
    template {
      metadata {
        labels = {
          "app.kubernetes.io/name"      = "mastodon"
          "app.kubernetes.io/instance"  = local.instance_name
          "app.kubernetes.io/component" = "web"
          "app.kubernetes.io/part-of"   = "rails"
          "mastodon/statsd-exporter"    = "true"
        }
        annotations = {
          config_change = sha1(jsonencode(merge(
            kubernetes_config_map.env.data,
            kubernetes_secret.env.data
          )))
        }
      }

      spec {
        container {
          name  = "web"
          image = "ghcr.io/mastodon/mastodon:${var.mastodon_version}"
          command = [
            "bundle",
            "exec",
            "puma",
            "-C",
            "config/puma.rb"
          ]

          env_from {
            config_map_ref {
              name = kubernetes_config_map.env.metadata.0.name
            }
          }

          env_from {
            secret_ref {
              name = kubernetes_secret.env.metadata.0.name
            }
          }

          env {
            name  = "PORT"
            value = "3000"
          }

          env {
            name  = "MIN_THREADS"
            value = "5"
          }

          env {
            name  = "MAX_THREADS"
            value = "5"
          }

          env {
            name  = "WEB_CONCURRENCY"
            value = "2"
          }

          env {
            name  = "PERSISTENT_TIMEOUT"
            value = "20"
          }

          port {
            name           = "http"
            container_port = 3000
            protocol       = "TCP"
          }

          liveness_probe {
            tcp_socket {
              port = "http"
            }
          }

          readiness_probe {
            http_get {
              path = "/health"
              port = "http"
            }
            timeout_seconds = 5
          }

          startup_probe {
            http_get {
              path = "/health"
              port = "http"
            }
            failure_threshold = 30
            period_seconds    = 5
          }

          resources {
            requests = var.web_resources.requests
            limits   = var.web_resources.limits
          }
        }

        topology_spread_constraint {
          max_skew           = 1
          topology_key       = "kubernetes.io/hostname"
          when_unsatisfiable = "ScheduleAnyway"
          label_selector {
            match_labels = {
              "app.kubernetes.io/component" = "web"
            }
          }
        }

        security_context {
          run_as_user  = 991
          fs_group     = 991
          run_as_group = 991
        }
      }
    }

    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_surge       = "0"
        max_unavailable = "1"
      }
    }
  }

  lifecycle {
    ignore_changes = [spec.0.replicas]
  }

  depends_on = [kubernetes_job.pre_migrations]
}
