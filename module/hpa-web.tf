resource "kubernetes_horizontal_pod_autoscaler_v2" "web" {
  metadata {
    name      = "web"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
  }

  spec {
    min_replicas = 1
    max_replicas = 10

    scale_target_ref {
      api_version = "apps/v1"
      kind        = "Deployment"
      name        = kubernetes_deployment.web.metadata.0.name
    }

    metric {
      type = "External"
      external {
        metric {
          name = "nginx_ingress_controller_requests_second"
          selector {
            match_labels = {
              ingress = kubernetes_ingress_v1.mastodon.metadata.0.name
            }
          }
        }
        target {
          type          = "AverageValue"
          average_value = 6
        }
      }
    }
  }
}
