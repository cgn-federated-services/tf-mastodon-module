resource "kubernetes_deployment" "mastodon_exporter" {
  metadata {
    name      = "mastodon-exporter"
    namespace = kubernetes_namespace.mastodon.metadata.0.name

    labels = {
      "app.kubernetes.io/name"      = "mastodon-exporter"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "exporter"
    }
  }

  spec {
    replicas = 0

    selector {
      match_labels = {
        "app.kubernetes.io/name"      = "mastodon-exporter"
        "app.kubernetes.io/instance"  = local.instance_name
        "app.kubernetes.io/component" = "exporter"
      }
    }

    template {
      metadata {
        labels = {
          "app.kubernetes.io/name"      = "mastodon-exporter"
          "app.kubernetes.io/instance"  = local.instance_name
          "app.kubernetes.io/component" = "exporter"
        }
      }

      spec {
        container {
          name  = "mastodon-exporter"
          image = "docker.io/systemli/prometheus-mastodon-exporter:1.0.5"
          args = [
            "-mastodon-url=https://${var.instance_hostname}"
          ]

          port {
            name           = "metrics"
            container_port = 13120
          }
        }
      }
    }
  }
}
