resource "kubernetes_ingress_v1" "mastodon" {
  metadata {
    name      = "mastodon-${local.instance_name}"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
    labels = {
      "app.kubernetes.io/name"     = "mastodon"
      "app.kubernetes.io/instance" = local.instance_name
    }
    annotations = {
      "cert-manager.io/cluster-issuer"                   = "letsencrypt-production"
      "nginx.ingress.kubernetes.io/from-to-www-redirect" = "true"
      "nginx.ingress.kubernetes.io/proxy-body-size"      = "200m"
      "nginx.ingress.kubernetes.io/enable-access-log"    = "false"
    }
  }
  spec {
    tls {
      hosts       = [var.instance_hostname]
      secret_name = "tls-${local.instance_name}"
    }

    ingress_class_name = "nginx"

    rule {
      host = var.instance_hostname
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service.web.metadata.0.name
              port {
                number = 3000
              }
            }
          }
        }
        path {
          path      = "/api/v1/streaming"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service.streaming.metadata.0.name
              port {
                number = 4000
              }
            }
          }
        }
      }
    }
  }
}
