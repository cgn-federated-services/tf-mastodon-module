resource "kubernetes_deployment" "streaming" {
  metadata {
    name      = "streaming"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
    labels = {
      "app.kubernetes.io/name"      = "mastodon"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "streaming"
    }
  }
  spec {
    selector {
      match_labels = {
        "app.kubernetes.io/name"      = "mastodon"
        "app.kubernetes.io/instance"  = local.instance_name
        "app.kubernetes.io/component" = "streaming"
      }
    }
    template {
      metadata {
        labels = {
          "app.kubernetes.io/name"      = "mastodon"
          "app.kubernetes.io/instance"  = local.instance_name
          "app.kubernetes.io/component" = "streaming"

        }
        annotations = {
          config_change = sha1(jsonencode(merge(
            kubernetes_config_map.env.data
          )))
        }
      }
      spec {
        container {
          name              = "streaming"
          image             = "ghcr.io/mastodon/mastodon-streaming:${var.mastodon_version}"
          image_pull_policy = "IfNotPresent"
          command           = ["node", "./streaming/index.js"]

          env_from {
            config_map_ref {
              name = kubernetes_config_map.env.metadata.0.name
            }
          }

          env_from {
            secret_ref {
              name = kubernetes_secret.env.metadata.0.name
            }
          }

          env {
            name  = "PORT"
            value = "4000"
          }

          port {
            name           = "streaming"
            container_port = 4000
            protocol       = "TCP"
          }

          readiness_probe {
            http_get {
              path = "/api/v1/streaming/health"
              port = "streaming"
            }
          }

          liveness_probe {
            http_get {
              path = "/api/v1/streaming/health"
              port = "streaming"
            }
          }

          resources {
            requests = var.streaming_resources.requests
            limits   = var.streaming_resources.limits
          }
        }
        security_context {
          run_as_user  = 991
          fs_group     = 991
          run_as_group = 991
        }
      }
    }
  }

  depends_on = [kubernetes_job.pre_migrations]
}
