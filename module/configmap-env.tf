resource "kubernetes_config_map" "env" {
  metadata {
    name      = "mastodon-env"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
  }

  data = {
    # Postgres
    "DB_POOL"             = "${var.postgres_pool}"
    "DB_HOST"             = var.postgres_host
    "DB_PORT"             = var.pgbouncer_port
    "DB_USER"             = var.postgres_user
    "DB_NAME"             = var.postgres_name
    "PREPARED_STATEMENTS" = "false"
    # ElasticSearch/OpenSearch
    "ES_ENABLED"   = "${var.elasticsearch_enabled}"
    "ES_PRESET"    = "single_node_cluster"
    "ES_HOST"      = var.elasticsearch_host
    "ES_PORT"      = "9200"
    "ES_PREFIX"    = var.elasticsearch_prefix
    "LOCAL_DOMAIN" = var.instance_hostname
    # https://devcenter.heroku.com/articles/tuning-glibc-memory-behavior
    "MALLOC_ARENA_MAX" = "2"
    "NODE_ENV"         = "production"
    "RAILS_ENV"        = "production"
    "RAILS_LOG_LEVEL"  = "warn"
    # Redis
    "REDIS_HOST" = var.redis_host
    "REDIS_PORT" = var.redis_port
    # Objectorage
    "S3_ENABLED"            = "${var.s3_enabled}"
    "S3_BUCKET"             = var.s3_bucket
    "S3_ENDPOINT"           = var.s3_endpoint
    "S3_REGION"             = var.s3_region
    "S3_ALIAS_HOST"         = var.s3_alias_host
    "S3_BATCH_DELETE_RETRY" = "10"
    # Assets
    "CDN_HOST" = var.cdn_host
    # Email
    "SMTP_AUTH_METHOD"          = "plain"
    "SMTP_DELIVERY_METHOD"      = "smtp"
    "SMTP_SERVER"               = var.smtp_server
    "SMTP_DOMAIN"               = var.smtp_domain
    "SMTP_ENABLE_STARTTLS"      = "never"
    "SMTP_ENABLE_STARTTLS_AUTO" = "false"
    "SMTP_OPENSSL_VERIFY_MODE"  = "none"
    "SMTP_FROM_ADDRESS"         = "Mastodon <noreply@${var.smtp_domain}>"
    "SMTP_PORT"                 = "465"
    "SMTP_TLS"                  = "true"
    # Translation
    "DEEPL_PLAN"          = "free"
    "IP_RETENTION_PERIOD" = "0"
  }
}
