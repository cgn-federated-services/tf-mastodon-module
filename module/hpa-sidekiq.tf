resource "kubernetes_horizontal_pod_autoscaler_v2" "sidekiq" {
  for_each = var.sidekiq_workers
  metadata {
    name      = "sidekiq-${each.key}"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
  }

  spec {
    min_replicas = each.value.hpa.min_replicas
    max_replicas = each.value.hpa.max_replicas

    scale_target_ref {
      api_version = "apps/v1"
      kind        = "Deployment"
      name        = kubernetes_deployment.sidekiq[each.key].metadata.0.name
    }

    metric {
      type = "External"
      external {
        metric {
          name = "sidekiq_queue_enqueued_jobs"
          selector {
            match_labels = {
              namespace = kubernetes_namespace.mastodon.metadata.0.name
            }
          }
        }
        target {
          type  = "Value"
          value = "1k"
        }
      }
    }
  }
}
