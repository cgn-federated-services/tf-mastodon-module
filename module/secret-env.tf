resource "kubernetes_secret" "env" {
  metadata {
    name      = "mastodon-env"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
  }

  data = {
    "SECRET_KEY_BASE"                              = var.secret_key_base
    "OTP_SECRET"                                   = var.otp_secret
    "VAPID_PRIVATE_KEY"                            = var.vapid_private_key
    "VAPID_PUBLIC_KEY"                             = var.vapid_public_key
    "ACTIVE_RECORD_ENCRYPTION_DETERMINISTIC_KEY"   = var.encryption_deterministic_key
    "ACTIVE_RECORD_ENCRYPTION_KEY_DERIVATION_SALT" = var.encryption_key_derivation_salt
    "ACTIVE_RECORD_ENCRYPTION_PRIMARY_KEY"         = var.encryption_primary_key
    "AWS_ACCESS_KEY_ID"                            = var.aws_access_key_id
    "AWS_SECRET_ACCESS_KEY"                        = var.aws_secret_access_key
    "DEEPL_API_KEY"                                = var.deepl_api_key
    "DB_PASS"                                      = var.postgres_password
  }
}
