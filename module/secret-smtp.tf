resource "kubernetes_secret" "smtp" {
  metadata {
    name      = "smtp"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
  }

  data = {
    "SMTP_LOGIN"    = var.smtp_login
    "SMTP_PASSWORD" = var.smtp_password
  }
}
