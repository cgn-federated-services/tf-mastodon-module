resource "kubernetes_deployment" "sidekiq" {
  for_each = var.sidekiq_workers
  metadata {
    name      = "sidekiq-${each.key}"
    namespace = kubernetes_namespace.mastodon.metadata.0.name
    labels = {
      "app.kubernetes.io/name"      = "mastodon"
      "app.kubernetes.io/instance"  = local.instance_name
      "app.kubernetes.io/component" = "sidekiq-${each.key}"
      "app.kubernetes.io/part-of"   = "rails"
    }
  }
  spec {
    selector {
      match_labels = {
        "app.kubernetes.io/name"      = "mastodon"
        "app.kubernetes.io/instance"  = local.instance_name
        "app.kubernetes.io/component" = "sidekiq-${each.key}"
        "app.kubernetes.io/part-of"   = "rails"
      }
    }
    template {
      metadata {
        labels = {
          "app.kubernetes.io/name"      = "mastodon"
          "app.kubernetes.io/instance"  = local.instance_name
          "app.kubernetes.io/component" = "sidekiq-${each.key}"
          "app.kubernetes.io/part-of"   = "rails"
          "mastodon/statsd-exporter"    = "true"
        }
        annotations = {
          config_change = sha1(jsonencode(merge(
            kubernetes_config_map.env.data,
            kubernetes_secret.env.data
          )))
        }
      }
      spec {

        volume {
          name = "workers"

          config_map {
            name = kubernetes_config_map.workers.metadata.0.name
          }
        }
        container {
          name              = "sidekiq"
          image             = "ghcr.io/mastodon/mastodon:${var.mastodon_version}"
          image_pull_policy = "IfNotPresent"
          command = concat(
            [
              "bundle",
              "exec",
              "sidekiq",
              "-c",
              "${each.value.concurrency}"
            ],
            each.value.queues
          )

          env_from {
            config_map_ref {
              name = kubernetes_config_map.env.metadata.0.name
            }
          }

          env_from {
            secret_ref {
              name = kubernetes_secret.env.metadata.0.name
            }
          }

          env_from {
            secret_ref {
              name = kubernetes_secret.smtp.metadata.0.name
            }
          }

          volume_mount {
            name       = "workers"
            mount_path = "/opt/mastodon/config/sidekiq.yml"
            sub_path   = "sidekiq.yml"
          }

          env {
            name  = "DB_POOL"
            value = each.value.db_pool
          }

          # Slow S3 API (Hetzner)
          env {
            name  = "S3_READ_TIMEOUT"
            value = "40"
          }

          resources {
            requests = each.value.resources.requests
            limits   = each.value.resources.limits
          }
        }

        security_context {
          run_as_user  = 991
          fs_group     = 991
          run_as_group = 991
        }
      }
    }

    strategy {
      type = "Recreate"
    }
  }

  depends_on = [kubernetes_job.pre_migrations]
}
