variable "instance_hostname" {
  description = "Hostname of the Mastodon instance"
  type        = string
}

variable "postgres_pool" {
  description = "DB Pool used by the web and sidekiq pods"
  type        = number
  default     = 50
}

variable "elasticsearch_enabled" {
  description = "Toggle Elasticsearch"
  type        = bool
  default     = false
}

variable "elasticsearch_prefix" {
  description = "Elasticserach Prefix for the instance"
  type        = string
  default     = null
}

variable "elasticsearch_host" {
  description = "Ip of Elasticserach host"
  type        = string
}

variable "s3_enabled" {
  description = "Toggle S3 storage of the instance"
  type        = bool
  default     = true
}

variable "s3_bucket" {
  description = "The s3 bucket of the instance"
  type        = string
}

variable "s3_endpoint" {
  description = "The s3 endpoint of the instance"
  type        = string
}

variable "s3_region" {
  description = "The s3 region of the instance"
  type        = string
}

variable "s3_alias_host" {
  description = "The s3 alias host of the instance"
  type        = string
  default     = null
}

variable "aws_access_key_id" {
  description = "The s3 access key of the instance"
  type        = string
  sensitive   = true
}

variable "aws_secret_access_key" {
  description = "The s3 secret key of the instance"
  type        = string
  sensitive   = true
}

variable "smtp_domain" {
  description = "The smtp domain of the instance"
  type        = string
}

variable "smtp_server" {
  description = "The smtp server of the instance"
  type        = string
}

variable "smtp_password" {
  description = "The smtp password of the instance"
  type        = string
  sensitive   = true
}

variable "smtp_login" {
  description = "The smtp login of the instance"
  type        = string
}

variable "sidekiq_workers" {
  description = "The sidekiq workers of the instance"
  default = {
    "scheduler" = {
      concurrency = 1
      db_pool     = 1
      queues = [
        "-q",
        "scheduler"
      ]
      resources = {
        "requests" = {
          "cpu"    = "100m"
          "memory" = "400Mi"
        }
        "limits" = {
          "cpu"    = null
          "memory" = null
        }
      }
      hpa = {
        min_replicas = 1
        max_replicas = 1
      }
    }
    "all" = {
      concurrency = 100
      db_pool     = 100
      queues = [
        "-q",
        "default,8",
        "-q",
        "push,6",
        "-q",
        "ingress,4",
        "-q",
        "mailers,2",
        "-q",
        "pull"
      ]
      resources = {
        "requests" = {
          "cpu"    = "1000m"
          "memory" = "1Gi"
        }
        "limits" = {
          "cpu"    = null
          "memory" = null
        }
      }
      hpa = {
        min_replicas = 1
        max_replicas = 3
      }
    }
  }
}

variable "mastodon_version" {
  description = "The mastodon version of the instance"
  type        = string
  # renovate: image=mastodon/mastodon registryUrl=https://ghcr.io
  default = "v4.3.4"
}

variable "secret_key_base" {
  description = "The secret key base of the instance"
  type        = string
  sensitive   = true
}

variable "otp_secret" {
  description = "The otp secret of the instance"
  type        = string
  sensitive   = true
}

variable "vapid_private_key" {
  description = "The vapid private key of the instance"
  type        = string
  sensitive   = true
}

variable "vapid_public_key" {
  description = "The vapid public key of the instance"
  type        = string
  sensitive   = true
}

variable "encryption_deterministic_key" {
  type      = string
  sensitive = true
}

variable "encryption_key_derivation_salt" {
  type      = string
  sensitive = true
}

variable "encryption_primary_key" {
  type      = string
  sensitive = true
}

variable "web_resources" {
  description = "Resources for the web pods"
  default = {
    "requests" = {
      "cpu"    = "1"
      "memory" = "1Gi"
    }
    "limits" = {
      "cpu"    = null
      "memory" = null
    }
  }
}

variable "streaming_resources" {
  description = "Resources for the streaming pods"
  default = {
    "requests" = {
      "cpu"    = "20m"
      "memory" = "100Mi"
    }
    "limits" = {
      "cpu"    = null
      "memory" = null
    }
  }
}

variable "deepl_api_key" {
  description = "The deepl api key"
  type        = string
  sensitive   = true
}

variable "postgres_password" {
  description = "Password of the Postgres User"
  type        = string
  sensitive   = true
}

variable "postgres_user" {
  description = "User of the Postgres database"
  type        = string
  default     = "mastodon"
}

variable "postgres_host" {
  description = "IP of the database host"
  type        = string
}

variable "postgres_port" {
  description = "Postgres port"
  type        = string
}

variable "postgres_name" {
  description = "Postgres database name"
  type        = string
  default     = "mastodon"
}

variable "pgbouncer_host" {
  description = "IP of the pgbouncer host"
  type        = string
}

variable "pgbouncer_port" {
  type = string
}

variable "redis_host" {
  description = "IP of the redis host"
  type        = string
}

variable "redis_port" {
  type = string
}

variable "cdn_host" {
  type = string
}

variable "vacuum_job_enabled" {
  type    = bool
  default = false
}
