
# TF Mastodon Module

This Tofu/Terraform module deploys a Mastodon instance. It depends on a external Postgres database and an external Elasticsearch cluster. It was created to run the Mastodon instances of [cgnfs.de](https://cgnfs.de).

## Generate Secrets

```bash
podman run ghcr.io/mastodon/mastodon:latest rake mastodon:webpush:generate_vapid_key
podman run ghcr.io/mastodon/mastodon:latest rake secret
podman run ghcr.io/mastodon/mastodon:latest rake secret
```